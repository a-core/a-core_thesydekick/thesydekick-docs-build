FROM ubuntu:18.04
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    tcsh \
    git \
    python3 \
    python3-pip \
    python3-sphinx \
    zlib1g-dev \
    libjpeg8-dev

# set up a sydekick project to act as the build environment
RUN git clone https://github.com/TheSystemDevelopmentKit/thesdk_template.git

# clone submodules over https
COPY clone-recursive-https.sh /
RUN ./clone-recursive-https.sh /thesdk_template

# install python dependencies
RUN cd /thesdk_template \
    && ./pip3userinstall.sh \

# configure project
RUN cd /thesdk_template \
    && ./configure
