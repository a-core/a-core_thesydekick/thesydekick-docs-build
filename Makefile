.PHONY: build-image

build-image:
	docker build --progress=plain -t thesydekick-docs-build .
